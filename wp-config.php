<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ukm' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*Iy}/7[=SUQe=:9?,D1@6EiH;KcpapvZ[[5lgO>nXdK-D@_Ho)#/[W,7lNVGb/U;' );
define( 'SECURE_AUTH_KEY',  'hwR.0a^L+,<e`Hp?v%t,1,nXxss0gC}nvJ*^/Rdbt4#l>S6YVP)A~xoCV;wAexEJ' );
define( 'LOGGED_IN_KEY',    'zk]ne{07E14I|{Fw6Zy+^=-s0)c*1n4g5chaG3l&X5nE|a/ey@y4.tF|z19MW~mH' );
define( 'NONCE_KEY',        'CC0G?R79TC=y5gn9lK6*g|w^j>?qA oA-tSu1^Yu^LYO&OUY+1V2K eF&>DvvX$#' );
define( 'AUTH_SALT',        'C*hz-E*h-?i+y4-+VlQ:YBJFTFPA(m;I=Q(Hx|_?8TCXl61G@L;1s;M3KVu0v^gD' );
define( 'SECURE_AUTH_SALT', ';v26}m{RQZwwL39<9iv$mfT;$ VGrnL]5n)OB4MZo+]Y+!ix&<HF#9/j|_,Y4A},' );
define( 'LOGGED_IN_SALT',   '*!rG6*ytG`g+)72nx s614jSay&ht._TU>zxgD4O[`+zz=uTWLJR?3BB@!c7^DIT' );
define( 'NONCE_SALT',       '6a#y~2>ZLcuAtSB8>OK3=./XC(366up^n1lv _K^bx9V<:Z*09@HM>a?,?1t6JTo' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
